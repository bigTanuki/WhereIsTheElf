import SwiftUI

struct ContentViewGeo: View {
    
    @State private var tappedCup: Set<Int> = []
    @State private var elfPosition = Int.random(in: -1..<2)
    @State private var cupsOffset = 150
    @State private var elfHidden = true
    @State private var youWin = false
    
    var body: some View {
        GeometryReader {geo in
            ZStack {
                
                Rectangle()
                    .fill(.cyan)
                Image("Elf")
                    .zIndex(youWin ? 1 : 0)
                    .scaleEffect(youWin ? 1 : 0.3, anchor: .bottom)
                    .animation(.easeIn(duration: 1.0), value: youWin )
                    .offset(elfHidden ? CGSize(width: geo.size.width/4 * -1, height: geo.size.width/4) : CGSize(width: Int(geo.size.width/4) * elfPosition, height: -5))
                //.onTapGesture {
                //elfHidden.toggle()
                //}
                
                Image("NaokiCup")
                    .scaleEffect(0.5, anchor: tappedCup.contains(-1) ? .top : .bottom)
                    .offset(CGSize(width: geo.size.width/4 * -1, height: 0))
                    .animation(.linear(duration: 1), value: tappedCup.contains(-1) )
                    .onTapGesture {
                        tappedCup.insert(-1)
                        performAction()
                    }
                Image("NaokiCup")
                    .scaleEffect(0.5, anchor: tappedCup.contains(0) ? .top : .bottom)
                    .offset(CGSize(width: geo.size.width/4 * 0, height: 0))
                    .animation(.linear(duration: 1), value: tappedCup.contains(0) )
                    .onTapGesture {
                        tappedCup.insert(0)
                        performAction()
                    }
                Image("NaokiCup")
                    .scaleEffect(0.5, anchor: tappedCup.contains(1) ? .top : .bottom)
                    .offset(CGSize(width: geo.size.width/4 * 1, height: 0))
                    .animation(.linear(duration: 1), value: tappedCup.contains(1) )
                    .onTapGesture {
                        tappedCup.insert(1)
                        performAction()
                    }
                
                Capsule()
                    .fill(.yellow)
                    .frame(width: 150, height: 50)
                    .offset(CGSize(width: 0, height: 400))
                
                Button {
                    if youWin == true {
                        print("restarted")
                        youWin = false
                        elfHidden = true
                        elfPosition = Int.random(in: -1..<2)
                        print(elfPosition)
                        tappedCup = []
                    } else if youWin == false {
                        elfHidden.toggle()
                        
                    }
                    
                }label: {
                    Label(youWin ? "Restart" : "start",  systemImage: "arrow.2.squarepath")
                } .offset(CGSize(width: 0, height: 400))
                
                
            }
            
        }
            
            
    }
        
    func performAction() {
        for cup in tappedCup {
            if cup == elfPosition {
                youWin = true
                print("You win")
            }
        }                
    }
}




