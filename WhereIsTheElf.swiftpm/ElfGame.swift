import SwiftUI

struct CupView: View {
    var body: some View {
        Image("NaokiCup")
    }
}

struct CupView_Previews: PreviewProvider {
    static var previews: some View {
        CupView()
    }
}

struct ElfGame: View {
    
    @State private var tappedCup: Set<Int> = []
    @State private var elfPosition = Int.random(in: -1..<2)
    @State private var cupsOffset = 150
    @State private var elfHidden = false
    @State private var youWin = false
    
    var body: some View {
        
        ZStack {
            
            Rectangle()
                .fill(.cyan)
            Image("Elf")
                .resizable()
                .frame(width: youWin ? 210 : 70, height: youWin ? 300 : 100)
                .zIndex(youWin ? 1 : 0)
                .animation(.easeIn(duration: 1.0), value: youWin )
                .offset(elfHidden ? CGSize(width: cupsOffset * elfPosition, height: 0) : CGSize(width: cupsOffset * -1, height: cupsOffset))
               
            HStack{
                
            }
            CupView()
                .scaleEffect(0.5, anchor: .center)
                .offset(CGSize(width: cupsOffset * -1, height: tappedCup.contains(-1) ? -150 : 0))
                .animation(.linear(duration: 1), value: tappedCup.contains(-1) )
                .onTapGesture {
                    tappedCup.insert(-1)
                    checkWin()
                }
            CupView()
                .scaleEffect(0.5, anchor: .center)
                .offset(CGSize(width: cupsOffset * 0, height: tappedCup.contains(0) ? -150 : 0))
                .animation(.linear(duration: 1), value: tappedCup.contains(0) )
                .onTapGesture {
                    tappedCup.insert(0)
                    checkWin()
                }
            CupView()
                .scaleEffect(0.5, anchor: .center)
                .offset(CGSize(width: cupsOffset * 1, height: tappedCup.contains(1) ? -150 : 0))
                .animation(.linear(duration: 1), value: tappedCup.contains(1) )
                .onTapGesture {
                    tappedCup.insert(1)
                    checkWin()
                }
            
            Capsule()
                .fill(.yellow)
                .frame(width: 150, height: 50)
                .offset(CGSize(width: 0, height: cupsOffset*2))
            
                
            
            Button {
                if youWin == true {
                    print("restarted")
                    youWin = false
                    elfHidden = false
                    elfPosition = Int.random(in: -1..<2)
                    print(elfPosition)
                    tappedCup = []
                } else if youWin == false {
                    elfHidden.toggle()
                    
                }
                
                
            }label: {
                Label(youWin ? "Restart" : "Start",  systemImage: "arrow.2.squarepath")
            } .offset(CGSize(width: 0, height: cupsOffset*2))
            
            
            
        }.ignoresSafeArea()
        
    }
    func checkWin() {
        for cup in tappedCup {
            if cup == elfPosition {
                youWin = true
                print("You win")
            }
        }
        
        
    }
}


