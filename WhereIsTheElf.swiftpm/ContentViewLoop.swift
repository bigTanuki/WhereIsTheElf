import SwiftUI
// Credit: This project was inspired by SmallDog's original find project and SalmonKings cup textures from "Find the Elf."

struct ContentViewLoop: View {
    
    @State private var tappedCup: Set<Int> = []
    @State private var elfPosition = Int.random(in: -1..<2)
    //@State private var numbers = [-1,0,1]
    @State private var cupsOffset = 150
    @State private var elfHidden = true
    @State private var youWin = false
    @State private var hideTitle = false
    
    var body: some View {
        
        ZStack {
            
            Rectangle()
                .fill(.cyan)
            
            Image("Elf")
                .zIndex(youWin ? 1 : 0)
                .scaleEffect(youWin ? 1 : 0.3, anchor: .bottom)
                .animation(.easeIn(duration: 1.0), value: youWin )
                .offset(elfHidden ? CGSize(width: cupsOffset * -1, height: 100) : CGSize(width: cupsOffset * elfPosition, height: -5))
            
            ForEach ((-1...1), id: \.self) {number in 
                Image("NaokiCup")
                    .scaleEffect(0.5, anchor: tappedCup.contains(number) ? .top : .bottom)
                    .offset(CGSize(width: cupsOffset * number, height: 0))
                    .animation(.linear(duration: 1), value: tappedCup.contains(number) )
                    .onTapGesture {
                        tappedCup.insert(number)
                        checkWin()
                    }
            }
            
            Capsule()
                .fill(.yellow)
                .frame(width: 150, height: 50)
                .offset(CGSize(width: 0, height:-200))
            
            Button {
                if youWin == true {
                    print("restarted")
                    youWin = false
                    elfHidden = true
                    elfPosition = Int.random(in: -1..<2)
                    print(elfPosition)
                    tappedCup = []
                } else if youWin == false {
                    elfHidden.toggle()
                    
                }
                
                
            }label: {
                Label(youWin ? "Restart" : "start",  systemImage: "arrow.2.squarepath")
            } .offset(CGSize(width: 0, height: -200))
            Text("Game Inspired by SmallDog \n and Cups design by Salmon King.")
                .font(.largeTitle)
                .multilineTextAlignment(.center)
                .foregroundColor(.white)
                .opacity(hideTitle ? 0 : 1)
                .onTapGesture {
                    hideTitle.toggle()
                }
            
            
            
            
            
            
        }
        
    }
    func checkWin() {
        for cup in tappedCup {
            if cup == elfPosition {
                youWin = true
                print("You win")
            }
        }
        
        
    }
}
