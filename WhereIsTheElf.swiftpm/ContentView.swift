import SwiftUI

struct ContentView: View {
    
    @State private var tappedCup: Set<Int> = []
    @State private var elfPosition = Int.random(in: 1..<4)
    
    var body: some View {
        ZStack {
            VStack{
                Spacer()
                
                Image("Elf")
                        .scaleEffect(0.3, anchor: .bottom)
                }
            
            
            HStack{
                Image("RedCup")
                    .scaleEffect(0.5, anchor: tappedCup.contains(1) ? .top : .bottom)
                    .animation(.linear(duration: 1), value: tappedCup.contains(1) )
                    .onTapGesture {
                        tappedCup.insert(1)
                        performAction()
                    }
                Image("RedCup")
                    .scaleEffect(0.5, anchor: tappedCup.contains(2) ? .top : .bottom)
                    .animation(.linear(duration: 1), value: tappedCup.contains(2) )
                    .onTapGesture {
                        tappedCup.insert(2)
                        performAction()
                    }
                Image("RedCup")
                    .scaleEffect(0.5, anchor: tappedCup.contains(3) ? .top : .bottom)
                    .animation(.linear(duration: 1), value: tappedCup.contains(3) )
                    .onTapGesture {
                        tappedCup.insert(3)
                        performAction()
                    }
            }
            
        }
        
    }
    func performAction() {
        // perform some action
        
    }
}

